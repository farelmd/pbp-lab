from django.forms.widgets import DateInput
from lab_1.models import Friend
from django import forms


class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = '__all__'
        widgets = {
            'DOB': forms.DateInput(attrs={'type': 'date'})
        }
