from django.http import HttpResponseRedirect
from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm

# Create your views here.


def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)


def add_note(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NoteForm(request.POST or None)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            form.save()
            return HttpResponseRedirect('/lab-4')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = NoteForm()

    response = {'form': form}
    return render(request, 'lab4_form.html', response)


def note_list(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)
