import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  TextStyle style1 = TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.normal,
    color: Color(0xFFFFFFFF),
  );

  @override
  Widget build(BuildContext context) {
    const primaryColor1 = Color(0xFF152636);
    TextEditingController namaController = TextEditingController();
    TextEditingController emailController = TextEditingController();
    TextEditingController kontakController = TextEditingController();

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text('Infid'),
          backgroundColor: primaryColor1,
        ),
        drawer: Drawer(
          child: Container(
            color: primaryColor1,
            child: Column(
              children: <Widget>[
                ListTile(
                  title: Text('Home',
                      style: GoogleFonts.poppins(textStyle: style1)),
                  onTap: () {},
                ),
                ListTile(
                  title: Text(
                    'Vaksinasi COVID-19',
                    style: GoogleFonts.poppins(textStyle: style1),
                  ),
                  onTap: () {},
                ),
                ListTile(
                  title: Text(
                    'Layanan Isolasi Mandiri',
                    style: GoogleFonts.poppins(textStyle: style1),
                  ),
                  onTap: () {},
                ),
                ListTile(
                  title: Text(
                    'Rumah Sakit Rujukan',
                    style: GoogleFonts.poppins(textStyle: style1),
                  ),
                  onTap: () {},
                ),
                ListTile(
                  title: Text(
                    'Tim Pakar COVID-19',
                    style: GoogleFonts.poppins(textStyle: style1),
                  ),
                  onTap: () {},
                ),
                ListTile(
                  title: Text(
                    'Kontak Layanan',
                    style: GoogleFonts.poppins(textStyle: style1),
                  ),
                  onTap: () {},
                ),
                ListTile(
                  title: Text(
                    'Persebaran Data',
                    style: GoogleFonts.poppins(textStyle: style1),
                  ),
                  onTap: () {},
                ),
                ListTile(
                  title: Text(
                    'Edukasi Protokol',
                    style: GoogleFonts.poppins(textStyle: style1),
                  ),
                  onTap: () {},
                ),
              ],
            ),
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.only(top: 120, left: 24, right: 24),
          child: Center(
            child: Column(
              children: [
                Text(
                  'Tambah Kontak',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 20),
                Column(
                  children: [
                    TextFormField(
                      controller: namaController,
                      validator: (value) {
                        if (value!.isEmpty) {
                          print('Nama Tidak Boleh Kosong');
                        }
                      },
                      decoration: InputDecoration(
                        fillColor: Color(0xffF1F0F5),
                        filled: true,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide: BorderSide(),
                        ),
                        labelText: 'Nama',
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10, bottom: 10),
                      child: TextFormField(
                        controller: emailController,
                        decoration: InputDecoration(
                          fillColor: Color(0xffF1F0F5),
                          filled: true,
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                            borderSide: BorderSide(),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                            borderSide: BorderSide(),
                          ),
                          labelText: 'Email',
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 10),
                      child: TextFormField(
                        controller: kontakController,
                        decoration: InputDecoration(
                          fillColor: Color(0xffF1F0F5),
                          filled: true,
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                            borderSide: BorderSide(),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                            borderSide: BorderSide(),
                          ),
                          labelText: 'Nomor Kontak',
                        ),
                      ),
                    ),
                    RaisedButton(
                      onPressed: () {
                        print(
                            "Kontak ditambah\nNama: ${namaController.text}\nEmail: ${emailController.text}\nKontak: ${kontakController.text}");
                      },
                      color: Color(0xff00ADB5),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)),
                      child: Text(
                        'Tambah',
                        style: TextStyle(color: Colors.white),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
