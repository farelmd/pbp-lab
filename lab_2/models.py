from django.db import models

# Create your models here.


class Note(models.Model):
    to = models.CharField(max_length=100, blank=True)
    note_from = models.CharField('from', max_length=100, blank=True)
    title = models.CharField(max_length=200, blank=True)
    message = models.TextField(max_length=1000, blank=True)
