# Generated by Django 3.2.7 on 2021-10-11 09:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_2', '0002_auto_20210927_1154'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note',
            name='note_from',
            field=models.CharField(blank=True, max_length=100, verbose_name='from'),
        ),
    ]
