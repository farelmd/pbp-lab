| 1                       | XML                                                                                                           | JSON                                                                                                                                    |
| ----------------------- | ------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------- |
| Ekstensi file           | .xml                                                                                                          | .json                                                                                                                                   |
| Tipe Data yang Didukung | Mendukung banyak tipe data kompleks termasuk bagan, charts, dan tipe data non-primitif lainnya.               | JSON hanya mendukung string, angka, array Boolean, dan objek. Bahkan objek hanya dapat berisi tipe primitif.                            |
| Dukungan Array          | Tidak mendukung array secara langsung. Jika ingin menggunakan array, harus ditambahkan tag untuk setiap item. | Mendukung array                                                                                                                         |
| Orientasi               | XML berorientasi pada dokumen.                                                                                | JSON berorientasi pada data.                                                                                                            |
| Dukungan UTF            | XML mendukung encoding  UTF-8 dan UTF-16.                                                                     | JSON mendukung encoding UTF serta ASCII.                                                                                                |
| Dukungan Namespaces     | Mendukung namespaces, komentar dan metadata                                                                   | Tidak ada ketentuan untuk namespace,  menambahkan komentar atau menulis metadata                                                        |
| Pengolahan              | Bisa memroses dan memformat dokumen dan objek.                                                                | Tidak dapat memroses atau menghitung                                                                                                    |
| Kecepatan               | Besar dan lambat dalam penguraian,  menyebabkan transmisi data lebih lambat                                   | Sangat cepat karena ukuran file sangat kecil,  penguraian lebih cepat oleh mesin JavaScript  yang menyebabkan transfer data lebih cepat |



| 2 | XML                                                                                       | HTML                                 |
| - | ----------------------------------------------------------------------------------------- | ------------------------------------ |
|   | Case sensitive                                                                            | Case insensitive                     |
|   | Dapat menentukan tags sesuai kebutuhan                                                    | Memiliki tags yang sudah ditentukan  |
|   | Digunakan untuk menyimpan data                                                            | Digunakan untuk menampilkan data     |
|   | Dapat melakukan transfer data dari dan ke database                                        | Hanya menampilkan data               |
|   | Tidak dapat menggunakan whitespaces                                                       | Dapat menggunakan whitespaces        |
|   | Objek harus diekspresikan dengan conventions.  Kebanyakan menggunakan atribut dan elemen. | Terdapat native object support       |
|   | Memerlukan Closing tags                                                                   | Tidak selalu memerlukan Closing tags |


Sumber:

[https://hackr.io/blog/json-vs-xml](https://hackr.io/blog/json-vs-xml)

[https://www.javatpoint.com/html-vs-xml](https://www.javatpoint.com/html-vs-xml)

[https://www.guru99.com/xml-vs-html-difference.html](https://www.guru99.com/xml-vs-html-difference.html)

[https://www.geeksforgeeks.org/html-vs-xml/](https://www.geeksforgeeks.org/html-vs-xml/)
