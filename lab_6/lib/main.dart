import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Infid: Kontak',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Infid'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextStyle style1 = TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.normal,
    color: Color(0xFFFFFFFF),
  );

  TextStyle styleNamaKontak = TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.bold,
  );

  TextStyle styleIsiKontak = const TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.normal,
  );

  @override
  Widget build(BuildContext context) {
    const primaryColor1 = Color(0xFF152636);

    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
        backgroundColor: primaryColor1,
      ),
      drawer: Drawer(
        child: Container(
          color: primaryColor1,
          child: Column(
            children: <Widget>[
              ListTile(
                title:
                    Text('Home', style: GoogleFonts.poppins(textStyle: style1)),
                onTap: () {},
              ),
              ListTile(
                title: Text(
                  'Vaksinasi COVID-19',
                  style: GoogleFonts.poppins(textStyle: style1),
                ),
                onTap: () {},
              ),
              ListTile(
                title: Text(
                  'Layanan Isolasi Mandiri',
                  style: GoogleFonts.poppins(textStyle: style1),
                ),
                onTap: () {},
              ),
              ListTile(
                title: Text(
                  'Rumah Sakit Rujukan',
                  style: GoogleFonts.poppins(textStyle: style1),
                ),
                onTap: () {},
              ),
              ListTile(
                title: Text(
                  'Tim Pakar COVID-19',
                  style: GoogleFonts.poppins(textStyle: style1),
                ),
                onTap: () {},
              ),
              ListTile(
                title: Text(
                  'Kontak Layanan',
                  style: GoogleFonts.poppins(textStyle: style1),
                ),
                onTap: () {},
              ),
              ListTile(
                title: Text(
                  'Persebaran Data',
                  style: GoogleFonts.poppins(textStyle: style1),
                ),
                onTap: () {},
              ),
              ListTile(
                title: Text(
                  'Edukasi Protokol',
                  style: GoogleFonts.poppins(textStyle: style1),
                ),
                onTap: () {},
              ),
            ],
          ),
        ),
      ),

      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Kontak Layanan',
              textAlign: TextAlign.left,
              style: GoogleFonts.poppins(
                  textStyle: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              )),
            ),
            Text(
              'Daftar kontak yang dapat dihubungi berkaitan dengan COVID-19\n\n\n',
              textAlign: TextAlign.left,
              style: GoogleFonts.poppins(textStyle: styleIsiKontak),
            ),
            Text(
              'Layanan Pengaduan Program Keluarga Harapan Kementerian Sosial\n',
              textAlign: TextAlign.left,
              style: GoogleFonts.poppins(textStyle: styleNamaKontak),
            ),
            Text(
              'Email                    : pengaduan@pkh.kemsos.go.id\n\nNomor Kontak    : 1500299',
              textAlign: TextAlign.left,
              style: GoogleFonts.poppins(textStyle: styleIsiKontak),
            ),
            Text(
              '\n\nLayanan Pengaduan Bansos Kementerian Sosial\n',
              textAlign: TextAlign.left,
              style: GoogleFonts.poppins(textStyle: styleNamaKontak),
            ),
            Text(
              'Email                    : bansoscovid19@kemsos.go.id\n\nNomor Kontak    : 157',
              textAlign: TextAlign.left,
              style: GoogleFonts.poppins(textStyle: styleIsiKontak),
            ),
            Text(
              '\n\nInformasi lebih lanjut mengenai perpajakan\n',
              textAlign: TextAlign.left,
              style: GoogleFonts.poppins(textStyle: styleNamaKontak),
            ),
            Text(
              'Email                    : informasi@pajak.go.id\n\nNomor Kontak    : 1500200',
              textAlign: TextAlign.left,
              style: GoogleFonts.poppins(textStyle: styleIsiKontak),
            ),
          ],
        ),
      ),
      floatingActionButton: SizedBox(
        width: 70,
        height: 40,
        child: FloatingActionButton(
          onPressed: () {},
          backgroundColor: Color(0xFFdc3545),
          tooltip: 'Keluar',
          child: const Text('Keluar'),
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(
                  top: Radius.circular(5), bottom: Radius.circular(5))),
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
